# AVR-Arduino-BigBrother
<img src="images/SemiBook.png"><br />

บอร์ดพัฒนา AVR/Arduino นั้นเริ่มแพร่หลาย แต่ข้อจำกัดยังเป็นการเลือกประเภท MCU<br />
เพราะเบอร์เล็กจะมีจำนวน I/O น้อยกว่า และบางครั้งต้องใช้เบอร์ใหญ่จึงอาจจะไม่สะดวก<br />
หากมีการปรับเปลี่ยนเบอร์ที่ใช้ระหว่างการพัฒนา<br /><br />

ด้วยเหตุนี้ผู้จัดทำจึงได้ออกแบบบอร์ดพัฒนา microcontroller ตระกูล AVR ขึ้นมาใหม่<br />
แนวคิดคือสามารถพัฒนา AVR ได้ทั้งแบบเบอร์เล็ก (28 pin) และ เบอร์ใหญ่ (40 pin)<br />
นอกจากนี้ยังรองรับการพัฒนาด้วย Arduino และยังเป็นเครื่องเบิร์น firmware ในตัวเองได้ด้วย<br />

โดยเนื้อหาทั้งหมดได้เผยแพร่ไว้ในวารสาร `เซมิคอนดักเตอร์` ฉบับที่ 343 เดือนเมษายน 2553<br />
ในโครงงานเรื่อง "Duino Big Brother"<br />


# images
<img src="images/BigBrother-Back.png" width="480" height="360">
<img src="images/BigBrother-Side-notext.png" width="410" height="307"><br />
<img src="images/bitbang.png" width="410" height="307"><br />

# 3D Render
<img src="images/DuinoBigBrother-side.jpg" width="410" height="307">
<img src="images/DuinoBigBrother-top.jpg" width="410" height="307"><br />

`TKVR`
